package com.audit.controller;

import com.audit.model.CustomerDTO;
import com.audit.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    private final CustomerService customerService;

    @PostMapping(value = "", consumes = { "application/json", "application/xml" },
            produces = { "application/json", "application/xml" })
    public ResponseEntity<Void> createCustomer(@RequestBody @Valid CustomerDTO customerDTO) {

        customerService.saveCustomer(customerDTO);

        LOGGER.debug("Create customer");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "", consumes = { "application/json", "application/xml" },
            produces = { "application/json", "application/xml" })
    public ResponseEntity<Void> editCustomer(@RequestParam @Valid Long customerId, @RequestBody @Valid CustomerDTO customerDTO) {

        customerService.editCustomer(customerId, customerDTO);

        LOGGER.debug("Edit customer:: customerId{}", customerId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "", consumes = { "application/json", "application/xml" },
            produces = { "application/json", "application/xml" })
    public ResponseEntity<List<CustomerDTO>> viewCustomers() {

        List<CustomerDTO> customers = customerService.getCustomers();

        LOGGER.debug("Load all customers");
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

}
