package com.audit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAuditDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAuditDataApplication.class, args);
	}

}
