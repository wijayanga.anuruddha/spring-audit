package com.audit.service.impl;

import com.audit.model.Customer;
import com.audit.model.CustomerDTO;
import com.audit.repo.CustomerRepository;
import com.audit.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository customerRepository;

    @Override
    public void saveCustomer(CustomerDTO customerDTO) {

        LOGGER.info("save customer :: customerName - {}", customerDTO.getName());
        customerRepository.save(populateCustomer(customerDTO));
    }

    @Override
    public void editCustomer(Long customerId, CustomerDTO customerDTO) {

        Optional<Customer> customer = customerRepository.findById(customerId);
        if (customer.isPresent()) {
            Customer modifiedCustomer = populateCustomer(customerDTO);
            modifiedCustomer.setId(customerId);
            customerRepository.save(modifiedCustomer);
            LOGGER.info("modify customer :: customerId - {}", customerId);
        }
    }

    @Override
    public List<CustomerDTO> getCustomers() {

        Iterable<Customer> customers = customerRepository.findAll();
        List<CustomerDTO> customerDTOS = populateCustomerDTO(customers);

        LOGGER.info("Get all customers");
        return customerDTOS;
    }

    private Customer populateCustomer(CustomerDTO customerDTO) {

        return Customer.builder()
                .name(customerDTO.getName())
                .country(customerDTO.getCountry())
                .email(customerDTO.getEmail())
                .build();
    }

    private List<CustomerDTO> populateCustomerDTO(Iterable<Customer> customers) {

        List<CustomerDTO> customerDTOS = new ArrayList<>();
        for (Customer customer : customers) {
            customerDTOS.add(
                    CustomerDTO.builder()
                            .id(customer.getId())
                            .name(customer.getName())
                            .country(customer.getCountry())
                            .email(customer.getEmail())
                            .build()
            );
        }
        return customerDTOS;
    }
}
