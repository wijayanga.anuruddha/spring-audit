package com.audit.service;

import com.audit.model.Customer;
import com.audit.model.CustomerDTO;

import java.util.List;

public interface CustomerService {

    void saveCustomer(CustomerDTO customerDTO);

    void editCustomer(Long customerId, CustomerDTO customerDTO);

    List<CustomerDTO> getCustomers ();
}
