package com.audit.repo;

import com.audit.model.Customer;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;


public interface CustomerRepository extends CrudRepository<Customer, Long>, JpaSpecificationExecutor<Customer>  {

}
